angular.module('starter.services', [])

.factory('DateForm', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var dates = [{
       id: 1, img: 'img/anglais.png', title: 'Anglais professionnel', date: '28-01-2017', time: '1h45', datetime: '2017-01-28 01:45:00'},
      { id: 2, img: 'img/allemand.png', title: 'Allemand débutant', date: '21-02-2017', time: '10h30', datetime: '2017-02-15 10:30:00' },
      { id: 3, img: 'img/anglais.png', title: 'Anglais débutant', date: '15-02-2017', time: '10h30', datetime: '2017-02-15 10:30:00' },
      { id: 4, img: 'img/allemand.png', title: 'Allemand professionnel', date: '21-02-2017', time: '10h30', datetime: '2017-02-15 10:30:00' },
      { id: 5, img: 'img/allemand.png', title: 'Allemand avancé', date: '21-02-2017', time: '10h30', datetime: '2017-02-15 10:30:00' 
  }];

  return {
    all: function() {
      return dates;
    },
    remove: function(date) {
      chats.splice(dates.indexOf(date), 1);
    },
    get: function(dateId) {
      for (var i = 0; i < dates.length; i++) {
        if (dates[i].id === parseInt(dateId)) {
          return dates[i];
        }
      }
      return null;
    }
  };
})

.factory('Cours', function() {
  var cours = [
   
 {
        id: 0, description: 'Apprenez à vous présenter', cours: [{ mot: 'Bonjour', trad: 'Hello', img: 'img/bonjour.jpg' },
        { mot: 'Au revoir', trad: 'Good bye', img: 'img/aurevoir.jpg' },
        { mot: 'Comment tu t\'appelles ?', trad: 'What is your name ?', img: 'img/nom.jpg' },
        { mot: 'Je m\'appelle ', trad: 'My name is', img: 'img/name.jpg' }]
      },
      {
        id: 1, description: 'La famille', cours: [{ mot: 'La mère', trad: 'The mother', img: 'img/mere.jpg' },
        { mot: 'Le frère ', trad: 'The brother', img: 'img/frere.jpg' },
        { mot: 'La soeur', trad: 'The sister', img: 'img/soeur.jpg' },
        { mot: 'Le père', trad: 'The father', img: 'img/pere.jpg' }]
      },
      {
        id: 2, description: 'Echanger avec quelqu\'un', cours: [{ mot: 'Quelle heure est-il ?', trad: 'What time is it ?', img: 'img/heure.jpg' },
        { mot: 'Comment allez-vous ?', trad: 'How are you ?', img: 'img/comment.jpg' },
        { mot: 'Très bien', trad: 'Very well', img: 'img/bien.jpg' },
        { mot: 'Merci', trad: 'Thank you', img: 'img/merci.jpg' }]
      },
      {
        id: 3, description: 'Echanger avec quelqu\'un', cours: [{ mot: 'Bonne nuit', trad: 'Good night', img: 'img/bonnenuit.jpg' },
        { mot: 'Parfait', trad: 'Perfect', img: 'img/parfait.jpg' },
        { mot: 'S\'il vous plaît', trad: 'Please', img: 'img/please.jpg' },
        { mot: 'Bienvenue', trad: 'Welcome', img: 'img/bienvenue.jpg' }]
      }
  
  
  ];

  return {
    all: function() {
      return cours;
    },
    remove: function(cour) {
      cours.splice(cours.indexOf(cour), 1);
    },
    get: function(courId) {
      for (var i = 0; i < cours.length; i++) {
        if (cours[i].id === parseInt(courId)) {
          return cours[i];
        }
      }
      return null;
    }
  };
})


.factory('Exercices', function() {
  var exercices = [
   
    {
        id: 0, description: 'Apprenez à vous présenter', exercice: [{ id: 0, mot: 'Bonjour', trad: 'Hello', img: 'img/bonjour.jpg' },
        { id: 1, mot: 'Au revoir', trad: 'Good bye', img: 'img/aurevoir.jpg' },
        { id: 2, mot: 'Comment tu t\'appelles ?', trad: 'What is your name ?', img: 'img/nom.jpg' },
        { id: 3, mot: 'Je m\'appelle ', trad: 'My name is', img: 'img/name.jpg' }]
      },
      {
        id: 1, description: 'La famille', exercice: [{ id: 0, mot: 'La mère', trad: 'The mother', img: 'img/mere.jpg' },
        { id: 1, mot: 'Le frère ', trad: 'The brother', img: 'img/frere.jpg' },
        { id: 2, mot: 'La soeur', trad: 'The sister', img: 'img/soeur.jpg' },
        { id: 3, mot: 'Le père', trad: 'The father', img: 'img/pere.jpg' }]
      },
      {
        id: 2, description: 'Echanger avec quelqu\'un', exercice: [{ id: 0, mot: 'Quelle heure est-il ?', trad: 'What time is it ?', img: 'img/heure.jpg' },
        { id: 1, mot: 'Comment allez-vous ?', trad: 'How are you ?', img: 'img/comment.jpg' },
        { id: 2, mot: 'Très bien', trad: 'Very well', img: 'img/bien.jpg' },
        { id: 3, mot: 'Merci', trad: 'Thank you', img: 'img/merci.jpg' }]
      },
      {
        id: 3, description: 'Echanger avec quelqu\'un', exercice: [{ id: 0, mot: 'Bonne nuit', trad: 'Good night', img: 'img/bonnenuit.jpg' },
        { id: 1, mot: 'Parfait', trad: 'Perfect', img: 'img/parfait.jpg' },
        { id: 2, mot: 'S\'il vous plaît', trad: 'Please', img: 'img/please.jpg' },
        { id: 3, mot: 'Bienvenue', trad: 'Welcome', img: 'img/bienvenue.jpg' }]
      }
  
  
  ];

  return {
    all: function() {
      return exercices;
    },
    remove: function(exercice) {
      exercices.splice(exercices.indexOf(exercice), 1);
    },
    get: function(exerciceId) {
      for (var i = 0; i < exercices.length; i++) {
        if (exercices[i].id === parseInt(exerciceId)) {
          return exercices[i];
        }
      }
      return null;
    }
  };
})

;
