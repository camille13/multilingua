angular.module('starter.controllers', [])

  .controller('DashCtrl', function ($scope) { })
  .controller('MessageCtrl', function ($scope, $cordovaContacts, $ionicPlatform, $cordovaEmailComposer, $cordovaSms) {


    $scope.addContact = function () {

      
      var myContact = navigator.contacts.create({ "displayName": "Multilingua" });
      myContact.phoneNumbers = [new ContactField('mobile', '6471234567')];
      myContact.emails = [new ContactField('professionnel', 'contact-paul@multilingua.com')];
      myContact.save(contactSuccess, contactError);

      function contactSuccess() {
        alert("Le contact a été enregistré !")
      }

      function contactError(message) {
        alert('Une erreur s\'est produite : ' + message);
      }

    };
    $scope.call = function () {
      window.plugins.CallNumber.callNumber("0681267061", true)
        .then(() => console.log('Launched dialer!'))
        .catch(() => console.log('Error launching dialer'));

    };

    $scope.email = function () {

      document.addEventListener('deviceready', function () {
    // cordova.plugins.email is now available

$cordovaEmailComposer.isAvailable().then(function () {
        // is available
        alert("available");
      }, function () {
        // not available
        alert("not available");
      });
      var emailVar = {
        to: 'camille.palpacuer@gmail.com',
        subject: 'Mail subject',
        body: 'How are you? Nice greetings from Leipzig',
        isHtml: true
      };

$cordovaEmailComposer.openDraft(emailVar);
  
}, false);

 };
  


    $scope.pickContactUsingNativeUI = function () {
      $cordovaContacts.pickContact().then(function (contactPicked) {
        $scope.contact = contactPicked;
      });
    }
  })

  .controller('DateCtrl', function ($scope, $stateParams, DateForm) {
    $scope.list = DateForm.all();
  })

  .controller('CoursCtrl', function ($scope, $stateParams, Cours) {

    $scope.list = Cours.all();
    $scope.randomItem = $scope.list[Math.floor(Math.random() * $scope.list.length)];
  })


  .controller('ExercicesCtrl', function ($scope, $stateParams, $ionicPopup, Exercices) {
    $scope.list = Exercices.all();
    $scope.point = 0;
    var iExo = 0;
    // ON RECUPERE L'ID DE LA LECON SUIVI
    $scope.item = $stateParams.randomItemId;
    $scope.score = $stateParams.score;
    $scope.cours = $scope.list[$scope.item];

    if ($scope.score == "") {
      $scope.button = '<p>Bon courage !</p><a  class="button button-block button-calm icon-right ion-chevron-right " href="#/cours/' + $scope.item + '/' + $scope.score + '/exo">Démarrer</a>'; 


  }

    etoile($scope.score);

    function etoile($point) {
      if ($point == "0") {
        $scope.statut = "Râté vous êtes un(e) Debutant(e) !!! ";
        $scope.message = 'Continuez à faire des efforts et revoyez le cours.';
        $scope.star = '';
      }
      else if ($point == "1") {
        $scope.statut = "Râté vous êtes un(e) Debutant(e) !!! ";
        $scope.message = 'Continuez à faire des efforts et revoyez le cours.';
        $scope.star = '<h2><i class="icon ion-star icon-energized"></i><i class="icon ion-star"></i><i class="icon ion-star"></i><i class="icon ion-star"></i><i class="icon ion-star"></i></h2>';
      }
      else if ($point == "2") {
        $scope.statut = "Râté vous êtes un(e) Apprenti(e) !!! ";
        $scope.message = 'Continuez à faire des efforts et revoyez le cours.';
        $scope.star = '<h2><i class="icon ion-star icon-energized"></i><i class="icon ion-star icon-energized"></i><i class="icon ion-star"></i><i class="icon ion-star"></i><i class="icon ion-star"></i></h2>';
      }
      else if ($point == "3") {
        $scope.statut = "Félicitation vous êtes un(e) Pro !!";
        $scope.message = " Vous avez la moyenne.";
        $scope.star = '<h2><i class="icon ion-star icon-energized"></i><i class="icon ion-star icon-energized"></i><i class="icon ion-star icon-energized"></i><i class="icon ion-star"></i><i class="icon ion-star"></i></h2>';
      }
      else if ($point == "4") {
        $scope.statut = "Félicitation vous êtes un(e) Pro !!";
        $scope.message = " Vous avez presque réussi tous les exercices. ";
        $scope.star = '<h2><i class="icon ion-star icon-energized"></i><i class="icon ion-star icon-energized"></i><i class="icon ion-star icon-energized"></i><i class="icon ion-star icon-energized"></i><i class="icon ion-star"></i></h2>';
      }
      else if ($point == "5") {
        $scope.statut = "Parfait vous êtes un(e) Expert !!";
        $scope.message = " Vous avez réussi parfaitement tous les exercices. ";
        $scope.star = '<h2><i class="icon ion-star icon-energized" color="calm"></i><i class="icon ion-star icon-energized" color="calm"></i><i class="icon ion-star icon-energized" color="calm"></i><i class="icon ion-star icon-energized" color="calm"></i><i class="icon ion-star icon-energized" color="calm"></i></h2>';
      }
    }
  })

  .controller('ExercicesDetailCtrl', function ($scope, $stateParams, $ionicPopup, $state, Exercices) {

    $scope.list = Exercices.all();
    $scope.point = 0;
    $scope.iExo = 0;
    $scope.item = $stateParams.itemId;
    $scope.exo = $scope.list[$scope.item].exercice[0];

    function win() {
      $ionicPopup.show({
        title: '<h4>Bonne réponse +1 <b><i class="icon ion-star icon-energized" color="calm"></b></h4>',
        template: 'Bravo ! Vous avez donné la bonne réponse',
        buttons: [{
          text: 'Ok',
          type: 'button-balanced'
        }]
      });
      $scope.point = $scope.point + 1;
    }

    function loose(reponse) {
      $ionicPopup.show({
        title: '<h4>Mauvaise réponse </h4>',
        template: 'La réponse était : <b>' + reponse + '</b>',
        buttons: [
          {
            text: 'Ok',
            type: 'button-assertive'
          }]
      });
    }


    var length = $scope.list[$scope.item].exercice.length;

    function nextExo() {
      if ($scope.iExo <= 4) {
        $scope.iExo = $scope.iExo + 1;
        if ($scope.iExo == length + 1) {
          $state.go('tab.cours-exercices', { randomItemId: $scope.item, score: $scope.point });
        }
        else {
          if ($scope.iExo == length) {
            $scope.cours = $scope.list[Math.floor(Math.random() * $scope.list.length)];
            $scope.exo = $scope.list[$scope.cours.id].exercice[0];
          }
          else {
            $scope.exo = $scope.list[$scope.item].exercice[$scope.iExo];
          }
        }
      } else { $state.go('tab.cours-exercices', { randomItemId: $scope.item, score: $scope.point }); }
    }


    $scope.createReponse = function () {
      if (this.reponse.trim().toUpperCase() == $scope.exo.trad.trim().toUpperCase()) {
        this.reponse = '';
        win();
        nextExo();
      }
      else {
        this.reponse = '';
        loose($scope.exo.trad);
        nextExo();
      }
    };
    if ($scope.iExo >= 6) {
      $state.go('tab.cours-exercices', { randomItemId: $scope.item, score: $scope.point });
    }
  })

  .controller('AccountCtrl', ['$scope', '$rootScope', '$ionicPlatform', '$cordovaLocalNotification', function ($scope, $rootScope, $ionicPlatform, $cordovaLocalNotification) {
    $ionicPlatform.ready(function () {

      // ========== Scheduling
      var now = new Date().getTime();
      var _10SecondsFromNow = new Date(now + 10 * 1000);
      $scope.scheduleSingleNotification = function () {
        $cordovaLocalNotification.schedule({
          id: 1,
          title: 'Demain formation',
          text: 'Text here',
          badge: 1,
          at: _10SecondsFromNow,
          led: "FF0000"
        }).then(function (result) {
          console.log('Saisi!');
        });
      };
    });
  }]);
