angular.module('starter.push', ['ionic', 'ngCordova'])
  .run(function ($ionicPlatform, $cordovaLocalNotification, DateForm) {
    $ionicPlatform.ready(function () {
var date=DateForm.all();
for(var i= 0; i < date.length; i++)
{
     dateformation = new Date(date[i]['datetime']).getTime();
     _1HourFromDate = new Date(dateformation - (3600 * 1000));

cordova.plugins.notification.local.schedule({
        id: i,
        title: 'Rappel Formation dans une heure',
        text: date[i]['title']+'à '+date[i]['time'],
        badge: 1,
        at: _1HourFromDate,
        led: "FF0000"
      });

}
    });
  });